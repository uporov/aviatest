package d.uporov.android.directions

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.github.udy18rus.easyargs.getArg
import d.uporov.android.R
import d.uporov.android.base.BaseFragmentWithViewModel
import d.uporov.android.search.SearchDirectionResult
import d.uporov.domain.directions.*
import kotlinx.android.synthetic.main.fragment_directions.*

private const val SEARCH_DIRECTION_RC = 123

class DirectionsFragment : BaseFragmentWithViewModel<DirectionsViewModel>(DirectionsViewModel::class) {

    override val resourceId = R.layout.fragment_directions

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.selectedFromDirectionLiveData.observe(this, Observer(::onFromSelected))
        viewModel.selectedToDirectionLiveData.observe(this, Observer(::onToSelected))
    }

    override fun onResume() {
        super.onResume()
        chosenFromCity.setOnClickListener {
            viewModel.searchDirection(DirectionType.FROM, SEARCH_DIRECTION_RC)
            chosenFromCity.error = null
        }
        chosenToCity.setOnClickListener {
            viewModel.searchDirection(DirectionType.TO, SEARCH_DIRECTION_RC)
            chosenToCity.error = null
        }

        findButton.setOnClickListener { viewModel.findFlights() }
    }

    override fun onPause() {
        super.onPause()
        chosenFromCity.setOnClickListener(null)
        chosenToCity.setOnClickListener(null)
        findButton.setOnClickListener(null)
    }

    private fun onFromSelected(item: Airport) {
        chosenFromCity.text = "${item.city.city} (${item.code})"
    }

    private fun onToSelected(item: Airport) {
        chosenToCity.text = "${item.city.city} (${item.code})"
    }

    override fun onError(error: Throwable) {
        when (error) {
            is IncorrectFromDirectionException ->
                chosenFromCity.error = getString(R.string.need_to_choose_from_direction_error)
            is IncorrectToDirectionException ->
                chosenToCity.error = getString(R.string.need_to_choose_to_direction_error)
            is FromAndToAreEqualException ->
                Toast.makeText(context, R.string.from_and_to_are_equal_error, Toast.LENGTH_SHORT).show()
            else -> super.onError(error)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SEARCH_DIRECTION_RC) {
            fragmentManager?.popBackStack()
            if (resultCode == RESULT_OK && data != null) {
                val result = data.getArg<SearchDirectionResult>()
                viewModel.onDirectionChosen(result.directionType, result.chosenDirection, result.airportCode)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}