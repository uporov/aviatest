package d.uporov.android.map

import android.os.Bundle
import android.view.View
import androidx.core.graphics.drawable.toBitmap
import com.github.udy18rus.easyargs.getArg
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import d.uporov.android.R
import d.uporov.android.base.BaseFragment
import d.uporov.android.drawTextOver
import d.uporov.data.model.directions.CitiesItem
import d.uporov.domain.directions.Flight

class WaitingMapFragment : BaseFragment() {

    override val resourceId = R.layout.fragment_waiting_map

    private val flight by lazy { getArg<Flight>() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment)
            .getMapAsync(::onMapReady)
    }

    private fun onMapReady(map: GoogleMap) = with(map) {
        uiSettings.setAllGesturesEnabled(false)
        setOnMarkerClickListener { true }

        val fromPosition = flight.from.city.position
        val toPosition = flight.to.city.position

        // mr.kostyl'
        // Необходимо для внесения изменений только после того, как загрузится карта.
        // Другие колбэки движения мне не подошли, т.к. отрабатывают еще до установление конечных координат
        // Связано с тем, что для утсановки 3-4 координат Кривой Безье используются Point -
        // непосредственные отображения координат на экран смартфона.
        var wasInitialChange = false
        setOnCameraIdleListener {
            if (!wasInitialChange) {
                wasInitialChange = true
            } else {
                val bezierCoordinates = curveBezier(fromPosition, toPosition)
                addMarkers(fromPosition to flight.from.code, toPosition to flight.to.code)
                addPolyline(context!!, *bezierCoordinates.toTypedArray())
                startFlight(fromPosition, toPosition, bezierCoordinates)
            }
        }
        moveCamera(context!!, fromPosition, toPosition)
    }

    private fun GoogleMap.addMarkers(from: Pair<LatLng, String>, to: Pair<LatLng, String>) {
        addMarker(
            MarkerOptions()
                .position(from.first)
                .anchor(0.5f, 0.5f)
                .icon(airportMarkerIcon(from.second))
        )
        addMarker(
            MarkerOptions()
                .position(to.first)
                .anchor(0.5f, 0.5f)
                .icon(airportMarkerIcon(to.second))
        )
    }

    private fun GoogleMap.startFlight(from: LatLng, to: LatLng, coordinates: List<LatLng>) {
        addMarker(
            MarkerOptions()
                .position(from)
                .anchor(0.5f, 0.5f)
                .zIndex(1f)
                .icon(shuttleMarkerIcon())
        ).animate(from, to, coordinates)
    }

    private fun airportMarkerIcon(airportCode: String) = resources
        .getDrawable(R.drawable.shape_marker, null)
        .toBitmap()
        .drawTextOver(context!!, airportCode)
        .let(BitmapDescriptorFactory::fromBitmap)

    private fun shuttleMarkerIcon() = resources
        .getDrawable(R.drawable.ic_plane, null)
        .toBitmap()
        .let(BitmapDescriptorFactory::fromBitmap)


    private val CitiesItem.position get() = LatLng(location.lat, location.lon)
}