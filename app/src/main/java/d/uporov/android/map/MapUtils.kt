package d.uporov.android.map

import android.animation.ObjectAnimator
import android.animation.TypeEvaluator
import android.content.Context
import android.graphics.Color
import android.util.Property
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.SphericalUtil
import d.uporov.android.R
import d.uporov.android.dimensionPixelSize
import kotlin.math.absoluteValue
import kotlin.math.roundToInt
import kotlin.math.sqrt

private const val MARKER_ANIMATION_DURATION_MS = 10000L
private const val PADDING_SCREEN_FRACTION = 0.12

private const val BEZIER_CURVE_POINTS = 1000
private const val BEZIER_CURVE_ACCURACY = BEZIER_CURVE_POINTS.toDouble()

fun Marker.animate(
    startPosition: LatLng,
    finalPosition: LatLng,
    coordinates: List<LatLng>
) {
    fun getCoordinate(fraction: Float): LatLng {
        return (fraction * BEZIER_CURVE_POINTS)
            .roundToInt()
            .let(coordinates::getOrNull) ?: coordinates.last()
    }

    val typeEvaluator = TypeEvaluator<LatLng> { fraction, _, _ -> getCoordinate(fraction) }
    val property = Property.of(Marker::class.java, LatLng::class.java, "position")
    val animator = ObjectAnimator.ofObject(this, property, typeEvaluator, finalPosition)

    var lastPosition = startPosition
    animator.addUpdateListener {
        val newPosition = getCoordinate(it.animatedFraction)
        if (newPosition != lastPosition) {
            val heading = SphericalUtil.computeHeading(lastPosition, newPosition)
            rotation = heading.toFloat()
            lastPosition = newPosition
            when {
                it.animatedFraction < 0.05 -> alpha = it.animatedFraction * 20
                it.animatedFraction > 0.95 -> alpha = (1 - it.animatedFraction) * 20
                alpha != 1f -> alpha = 1f
            }
        }
    }
    animator.repeatCount = Integer.MAX_VALUE - 1
    animator.duration = MARKER_ANIMATION_DURATION_MS
    animator.start()
}

fun GoogleMap.addPolyline(context: Context, vararg coordinates: LatLng) {
    PolylineOptions()
        .apply { coordinates.forEach { add(it) } }
        .width(context.dimensionPixelSize(R.dimen.dotSize))
        .color(Color.GRAY)
        .pattern(listOf(Dot(), Gap(context.dimensionPixelSize(R.dimen.dotsGap))))
        .geodesic(false)
        .run(::addPolyline)
}

fun GoogleMap.moveCamera(context: Context, from: LatLng, to: LatLng) {
    val bounds = LatLngBounds.builder()
        .include(from)
        .include(to)
        // mr.kostyl'
        // При определенных координатах (например из Ижевска в Питер) метод moveCamera() работает некорректно:
        // показывается противоположное полушарие. Третья координата фиксит, беру середину.
        // Данные для воспроизведения:
        // val fromPosition = LatLng(56.855675, 53.201321) // Ижевск
        // val toPosition = LatLng(59.95, 30.316667) // СПб
        .include(SphericalUtil.interpolate(from, to, 0.5))
        .build()

    setOnMapLoadedCallback {
        val padding = (context.resources.displayMetrics.widthPixels * PADDING_SCREEN_FRACTION).toInt()
        animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding))
    }
}

fun GoogleMap.curveBezier(start: LatLng, end: LatLng): List<LatLng> {
    val startPoint = projection.toScreenLocation(start)
    val endPoint = projection.toScreenLocation(end)

    val dx = startPoint.x - endPoint.x
    val dy = startPoint.y - endPoint.y

    val distance = sqrt(((dx * dx) + (dy * dy)).toDouble())
    val longLeg = (distance / 2).roundToInt()
    val shortLeg = (distance / 3).roundToInt()

    when {
        dx >= 0 && dy >= 0 -> {
            if (dx > dy) {
                startPoint.offset(-shortLeg, -longLeg)
                endPoint.offset(shortLeg, longLeg)
            } else {
                startPoint.offset(-longLeg, -shortLeg)
                endPoint.offset(longLeg, shortLeg)
            }
        }
        dx >= 0 && dy < 0 -> {
            if (dx > dy.absoluteValue) {
                startPoint.offset(-shortLeg, longLeg)
                endPoint.offset(shortLeg, -longLeg)
            } else {
                startPoint.offset(-longLeg, shortLeg)
                endPoint.offset(longLeg, -shortLeg)
            }
        }

        dx < 0 && dy >= 0 -> {
            if (dx.absoluteValue > dy) {
                startPoint.offset(shortLeg, -longLeg)
                endPoint.offset(-shortLeg, longLeg)
            } else {
                startPoint.offset(longLeg, -shortLeg)
                endPoint.offset(-longLeg, shortLeg)
            }
        }
        dx < 0 && dy < 0 -> {
            if (dx.absoluteValue > dy.absoluteValue) {
                startPoint.offset(shortLeg, longLeg)
                endPoint.offset(-shortLeg, -longLeg)
            } else {
                startPoint.offset(longLeg, shortLeg)
                endPoint.offset(-longLeg, -shortLeg)
            }
        }
    }

    val pA = projection.fromScreenLocation(startPoint)
    val pB = projection.fromScreenLocation(endPoint)

    return 0.rangeTo(BEZIER_CURVE_POINTS)
        .map { it / BEZIER_CURVE_ACCURACY }
        .map {
            val arcX = ((1 - it) * (1 - it) * (1 - it) * start.latitude
                    + 3.0 * (1 - it) * (1 - it) * it * pA.latitude
                    + 3.0 * (1 - it) * it * it * pB.latitude
                    + it * it * it * end.latitude)
            val arcY = ((1 - it) * (1 - it) * (1 - it) * start.longitude
                    + 3.0 * (1 - it) * (1 - it) * it * pA.longitude
                    + 3.0 * (1 - it) * it * it * pB.longitude
                    + it * it * it * end.longitude)
            LatLng(arcX, arcY)
        }
        .toList()
}