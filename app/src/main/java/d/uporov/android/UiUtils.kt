package d.uporov.android

import android.content.Context
import android.graphics.*
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.DimenRes
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment


fun SearchView.doOnQuery(query: (String) -> Unit) {
    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            query(query)
            return false
        }

        override fun onQueryTextChange(newText: String): Boolean {
            query(newText)
            return true
        }
    })
}

fun SearchView.getEditText(): EditText {
    return findViewById(androidx.appcompat.R.id.search_src_text)
}

fun Bitmap.drawTextOver(context: Context, text: String): Bitmap {
    val bitmap = copy(config, true)

    val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.WHITE
        xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)
        textSize = context.dimensionPixelSize(R.dimen.destinationAirportNameSize)
        textScaleX = 1f
    }

    val canvas = Canvas(bitmap)

    val widths = FloatArray(text.length)
    paint.getTextWidths(text, widths)
    val x = (canvas.width - widths.sum()) / 2

    val bounds = Rect()
    paint.getTextBounds(text, 0, text.length, bounds)
    val y = ((canvas.height - bounds.height()).toFloat() / 2) + bounds.height()
    canvas.drawText(text, x, y, paint)
    return bitmap
}

fun Context.dimensionPixelSize(@DimenRes resId: Int) = resources.getDimensionPixelSize(resId).toFloat()

fun Fragment.showSoftInputFor(view: View) {
    ContextCompat.getSystemService(context!!, InputMethodManager::class.java)
        ?.showSoftInput(view, InputMethodManager.SHOW_FORCED)
}

fun Fragment.hideSoftInput() {
    ContextCompat.getSystemService(context!!, InputMethodManager::class.java)
        ?.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)
}