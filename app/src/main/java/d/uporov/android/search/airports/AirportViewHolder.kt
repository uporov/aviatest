package d.uporov.android.search.airports

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_airport.view.*

class AirportViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(code: String) {
        itemView.airportCode.text = code
    }
}