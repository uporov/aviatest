package d.uporov.android.search

import d.uporov.data.model.directions.CitiesItem
import d.uporov.domain.directions.DirectionType
import java.io.Serializable

data class SearchDirectionResult(
    val directionType: DirectionType,
    val chosenDirection: CitiesItem,
    val airportCode: String = chosenDirection.iata.getOrNull(0)
        ?: chosenDirection.city.take(3).toUpperCase()
) : Serializable