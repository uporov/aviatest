package d.uporov.android.search

import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import d.uporov.data.model.directions.CitiesItem
import kotlinx.android.synthetic.main.item_direction.view.*

class DirectionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(item: CitiesItem) {
        itemView.cityTv.text = item.city
        itemView.countryTv.text = item.country
        itemView.arrowView.isVisible = item.iata.count() > 1
    }
}