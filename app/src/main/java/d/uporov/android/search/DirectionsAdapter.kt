package d.uporov.android.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import d.uporov.android.R
import d.uporov.data.model.directions.CitiesItem

class DirectionsAdapter(
    private val onItemSelected: (CitiesItem) -> Unit
) : RecyclerView.Adapter<DirectionViewHolder>() {

    private var items: List<CitiesItem> = emptyList()

    fun clearAndAddAll(list: List<CitiesItem>) {
        items = list
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DirectionViewHolder {
        return LayoutInflater.from(parent.context)
            .inflate(R.layout.item_direction, parent, false)
            .let(::DirectionViewHolder)
            .also { vh ->
                vh.itemView.setOnClickListener {
                    vh.adapterPosition
                        .let(items::get)
                        .run(onItemSelected::invoke)
                }
            }
    }

    override fun onBindViewHolder(holder: DirectionViewHolder, position: Int) {
        holder.bind(items[position])
    }
}