package d.uporov.android.search.airports

import d.uporov.data.model.directions.CitiesItem
import java.io.Serializable

data class AirportChoosingResult(
    val chosenDirection: CitiesItem,
    val airportCode: String = chosenDirection.iata.getOrNull(0)
        ?: chosenDirection.city.take(3).toUpperCase()
) : Serializable