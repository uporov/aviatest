package d.uporov.android.search

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.udy18rus.easyargs.asArg
import com.github.udy18rus.easyargs.asIntent
import com.github.udy18rus.easyargs.getArg
import d.uporov.android.*
import d.uporov.android.base.BaseFragmentWithViewModel
import d.uporov.android.search.airports.AirportChoosingResult
import d.uporov.data.model.directions.CitiesItem
import d.uporov.domain.directions.DirectionType
import d.uporov.domain.search.Loading
import d.uporov.domain.search.SearchDirectionViewModel
import d.uporov.domain.search.SearchResult
import d.uporov.domain.search.SearchState
import kotlinx.android.synthetic.main.fragment_search_direction.*
import java.net.UnknownHostException

private const val CHOOSE_AIRPORT_RC = 124

class SearchDirectionFragment : BaseFragmentWithViewModel<SearchDirectionViewModel>(SearchDirectionViewModel::class) {

    override val resourceId = R.layout.fragment_search_direction

    private val directionType by lazy { getArg<DirectionType>() }
    private val directionsAdapter by lazy { DirectionsAdapter(::onDirectionSelected) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.searchStateLiveData.observe(this, Observer(::onNewSearchState))

        initViews()
    }

    private fun initViews() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = directionsAdapter

        searchView.queryHint = when (directionType) {
            DirectionType.FROM -> getString(R.string.from)
            DirectionType.TO -> getString(R.string.to)
        }
        searchView.doOnQuery(viewModel::findDirection)
    }

    private fun onNewSearchState(state: SearchState) {
        when (state) {
            Loading -> {
                progressBar.isVisible = true
                hintTv.isVisible = false
                recyclerView.isVisible = false
            }
            is SearchResult -> {
                progressBar.isVisible = false

                val list = state.list
                if (list.isEmpty()) {
                    hintTv.isVisible = true
                    recyclerView.isVisible = false

                    if (searchView.query.isNullOrBlank()) {
                        hintTv.setText(R.string.search_hint)
                    } else {
                        hintTv.setText(R.string.found_nothing)
                    }
                } else {
                    hintTv.isVisible = false
                    recyclerView.isVisible = true

                    directionsAdapter.clearAndAddAll(list)
                }
            }
        }
    }

    override fun onError(error: Throwable) {
        if (error is UnknownHostException) {
            hintTv.setText(R.string.no_network_error)
        } else {
            super.onError(error)
            hintTv.text = error.message ?: error::class.java.simpleName
        }
        progressBar.isVisible = false
        hintTv.isVisible = true
        recyclerView.isVisible = false
    }

    override fun onResume() {
        super.onResume()
        toolbar.setNavigationOnClickListener {
            targetFragment?.onActivityResult(targetRequestCode, RESULT_CANCELED, null)
            hideSoftInput()
        }
        searchView.getEditText().run {
            if (text.isNullOrBlank()) {
                requestFocus()
                showSoftInputFor(this)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        toolbar.setNavigationOnClickListener(null)
    }

    private fun onDirectionSelected(direction: CitiesItem) {
        hideSoftInput()
        if (direction.iata.count() > 1) {
            chooseAirport(direction)
        } else {
            SearchDirectionResult(directionType, direction)
                .run(::sendResult)
        }
    }

    private fun chooseAirport(citiesItem: CitiesItem) {
        viewModel.chooseAirport(citiesItem, CHOOSE_AIRPORT_RC)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CHOOSE_AIRPORT_RC) {
            fragmentManager?.popBackStack()
            if (resultCode == RESULT_OK && data != null) {
                data.getArg<AirportChoosingResult>()
                    .let { SearchDirectionResult(directionType, it.chosenDirection, it.airportCode) }
                    .run(::sendResult)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun sendResult(result: SearchDirectionResult) {
        result.asArg()
            .asIntent()
            .run { targetFragment?.onActivityResult(targetRequestCode, RESULT_OK, this) }
    }
}