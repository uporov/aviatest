package d.uporov.android.search.airports

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import d.uporov.android.R

class AirportsAdapter(
    private val airports: List<String>,
    private val onItemSelected: (String) -> Unit
) : RecyclerView.Adapter<AirportViewHolder>() {

    override fun getItemCount() = airports.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirportViewHolder {
        return LayoutInflater.from(parent.context)
            .inflate(R.layout.item_airport, parent, false)
            .let(::AirportViewHolder)
            .also { vh ->
                vh.itemView.setOnClickListener {
                    vh.adapterPosition
                        .let(airports::get)
                        .run(onItemSelected::invoke)
                }
            }
    }

    override fun onBindViewHolder(holder: AirportViewHolder, position: Int) {
        holder.bind(airports[position])
    }
}