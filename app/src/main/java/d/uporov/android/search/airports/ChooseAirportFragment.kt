package d.uporov.android.search.airports

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.udy18rus.easyargs.asArg
import com.github.udy18rus.easyargs.asIntent
import com.github.udy18rus.easyargs.getArg
import d.uporov.android.R
import d.uporov.android.base.BaseFragment
import d.uporov.data.model.directions.CitiesItem
import kotlinx.android.synthetic.main.fragment_choose_airport.*

class ChooseAirportFragment: BaseFragment() {

    override val resourceId = R.layout.fragment_choose_airport

    private val citiesItem by lazy { getArg<CitiesItem>() }
    private val adapter by lazy { AirportsAdapter(citiesItem.iata, ::onAirportSelected) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.title = getString(R.string.choose_airport, citiesItem.city)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        toolbar.setNavigationOnClickListener {
            targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_CANCELED, null)
        }
    }

    override fun onPause() {
        super.onPause()
        toolbar.setNavigationOnClickListener(null)
    }

    private fun onAirportSelected(airportCode: String) {
        AirportChoosingResult(citiesItem, airportCode)
            .asArg()
            .asIntent()
            .run { targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, this) }
    }
}