package d.uporov.android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.github.udy18rus.easyargs.asArg
import com.github.udy18rus.easyargs.newInstance
import d.uporov.android.directions.DirectionsFragment
import d.uporov.android.map.WaitingMapFragment
import d.uporov.android.search.SearchDirectionFragment
import d.uporov.android.search.airports.ChooseAirportFragment
import d.uporov.domain.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val viewModel by inject<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.navigationLiveData.observe(this, Observer(::navigateTo))
    }

    private fun navigateTo(target: NavigationTarget?) {
        if (target == null) {
            if (supportFragmentManager.findFragmentById(android.R.id.content) == null) {
                viewModel.initTarget()
            }
            return
        }
        viewModel.onNavigationProcessed()
        supportFragmentManager.beginTransaction()
            .apply {
                when (target) {
                    Directions ->
                        replace(android.R.id.content, newInstance<DirectionsFragment>())
                    is SearchDirection ->
                        setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .replace(android.R.id.content,
                                newInstance<SearchDirectionFragment>(target.directionType.asArg())
                                    .apply {
                                        setTargetFragment(
                                            supportFragmentManager.findFragmentById(android.R.id.content),
                                            target.requestCode
                                        )
                                    })
                            .addToBackStack(null)
                    is ChooseAirport ->
                        setCustomAnimations(
                            R.anim.slide_in_right,
                            R.anim.slide_out_left,
                            R.anim.slide_in_left,
                            R.anim.slide_out_right
                        )
                            .replace(android.R.id.content, newInstance<ChooseAirportFragment>(target.citiesItem.asArg())
                                .apply {
                                    setTargetFragment(
                                        supportFragmentManager.findFragmentById(android.R.id.content),
                                        target.requestCode
                                    )
                                })
                            .addToBackStack(null)
                    is WaitingMap ->
                        setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .replace(android.R.id.content, newInstance<WaitingMapFragment>(target.flight.asArg()))
                            .addToBackStack(null)
                }
            }.commit()
    }
}
