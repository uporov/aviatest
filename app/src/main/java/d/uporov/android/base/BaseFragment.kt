package d.uporov.android.base

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import d.uporov.android.R

abstract class BaseFragment : Fragment() {

    @get:LayoutRes
    protected abstract val resourceId: Int

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(resourceId, container, false)
    }

    // Override to render custom errors
    protected open fun onError(error: Throwable) {
        Log.e(error::class.java.simpleName, error.toString())
        Toast.makeText(context, R.string.undefined_error_message, Toast.LENGTH_SHORT).show()
    }
}