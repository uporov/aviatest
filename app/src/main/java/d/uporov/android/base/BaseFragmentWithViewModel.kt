package d.uporov.android.base

import androidx.lifecycle.Observer
import d.uporov.domain.base.BaseViewModel
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.reflect.KClass

abstract class BaseFragmentWithViewModel<T : BaseViewModel>(vmClass: KClass<T>) : BaseFragment() {

    protected val viewModel: T by lazy {
        getViewModel(vmClass)
            .also { it.errorLiveData.observe(this, Observer(::renderError)) }
    }

    private fun renderError(error: Throwable?) {
        error ?: return
        viewModel.errorWasRendered()
        onError(error)
    }
}