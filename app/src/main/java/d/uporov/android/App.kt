package d.uporov.android

import android.app.Application
import d.uporov.domain.domainKoinModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(*domainKoinModules)
        }
    }
}