package d.uporov.data.model.directions

import com.google.gson.annotations.SerializedName

data class HotelsItem(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("distance")
	val distance: Double? = null,

	@field:SerializedName("latinLocationFullName")
	val latinLocationFullName: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("timezone")
	val timezone: String? = null,

	@field:SerializedName("rating")
	val rating: Int? = null,

	@field:SerializedName("_score")
	val score: Double? = null,

	@field:SerializedName("latinName")
	val latinName: String? = null,

	@field:SerializedName("photos")
	val photos: List<Long?>? = null,

	@field:SerializedName("countryId")
	val countryId: Int? = null,

	@field:SerializedName("photoCount")
	val photoCount: Int? = null,

	@field:SerializedName("latinCity")
	val latinCity: String? = null,

	@field:SerializedName("locationId")
	val locationId: Int? = null,

	@field:SerializedName("state")
	val state: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("locationFullName")
	val locationFullName: String? = null,

	@field:SerializedName("latinClar")
	val latinClar: String? = null,

	@field:SerializedName("timezonesec")
	val timezonesec: Int? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("hotelFullName")
	val hotelFullName: String? = null,

	@field:SerializedName("stars")
	val stars: Int? = null,

	@field:SerializedName("latinCountry")
	val latinCountry: String? = null,

	@field:SerializedName("clar")
	val clar: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("locationHotelsCount")
	val locationHotelsCount: Any? = null
)