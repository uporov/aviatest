package d.uporov.data.model.directions

import com.google.gson.annotations.SerializedName

data class Location(

	@field:SerializedName("lon")
	val lon: Double,

	@field:SerializedName("lat")
	val lat: Double
)