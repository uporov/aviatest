package d.uporov.data.model.directions

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CitiesItem(

    @field:SerializedName("latinClar")
    val latinClar: String? = null,

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("timezonesec")
    val timezonesec: Int? = null,

    @field:SerializedName("city")
    val city: String,

    @field:SerializedName("timezone")
    val timezone: String? = null,

    @field:SerializedName("hotelsCount")
    val hotelsCount: Int? = null,

    @field:SerializedName("_score")
    val score: Int? = null,

    @field:SerializedName("countryId")
    val countryId: Int? = null,

    @field:SerializedName("latinCity")
    val latinCity: String? = null,

    @field:SerializedName("latinCountry")
    val latinCountry: String? = null,

    @field:SerializedName("latinFullName")
    val latinFullName: String? = null,

    @field:SerializedName("clar")
    val clar: String? = null,

    @field:SerializedName("iata")
    val iata: List<String> = emptyList(),

    @field:SerializedName("countryCode")
    val countryCode: String? = null,

    @field:SerializedName("location")
    val location: Location,

    @field:SerializedName("fullname")
    val fullname: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("state")
    val state: Any? = null
) : Serializable {

    override fun toString(): String {
        return "$city ($country)"
    }
}