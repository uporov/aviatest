package d.uporov.data.model.directions

import com.google.gson.annotations.SerializedName

data class DirectionsResponse(

	@field:SerializedName("cities")
	val cities: List<CitiesItem> = emptyList(),

	@field:SerializedName("hotels")
	val hotels: List<HotelsItem> = emptyList()
)