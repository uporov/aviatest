package d.uporov.data.interactors

import d.uporov.data.AviasalesApi
import d.uporov.data.model.directions.CitiesItem
import d.uporov.data.model.directions.DirectionsResponse
import io.reactivex.Single

class QueryDirectionsInteractor(
    private val api: AviasalesApi
) : (String) -> Single<List<CitiesItem>> {

    override fun invoke(query: String): Single<List<CitiesItem>> {
        return if (query.isBlank()) {
            Single.just(emptyList())
        } else {
            api.findDirections(query)
                .map(DirectionsResponse::cities)
        }
    }
}