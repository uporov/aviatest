package d.uporov.data.interactors

import d.uporov.data.model.directions.CitiesItem
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

private const val SKIP_QUERY_TIMEOUT_MS = 200L

class DebouncedQueryDirectionsInteractor(
    private val queryDirectionsInteractor: QueryDirectionsInteractor
) : (String) -> Unit {

    private var querySubject = PublishSubject.create<String>()
    private var lastQuery: String? = null

    fun resultObservable(): Observable<List<CitiesItem>> {
        return querySubject
            .debounce(SKIP_QUERY_TIMEOUT_MS, TimeUnit.MILLISECONDS)
            .flatMap { query ->
                queryDirectionsInteractor(query)
                    .filter { query == lastQuery }
                    .subscribeOn(Schedulers.io())
                    .toObservable()
            }
    }

    override fun invoke(query: String) {
        lastQuery = query
        querySubject.onNext(query)
    }

    fun recreate(): Observable<List<CitiesItem>> {
        querySubject = PublishSubject.create()
        return resultObservable()
    }
}