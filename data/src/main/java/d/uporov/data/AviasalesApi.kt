package d.uporov.data

import d.uporov.data.model.directions.DirectionsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface AviasalesApi {

    @GET("autocomplete")
    fun findDirections(
        @Query("term") query: String,
        @Query("lang") lang: String = "ru"
    ) : Single<DirectionsResponse>
}