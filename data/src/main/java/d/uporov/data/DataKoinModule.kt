package d.uporov.data

import d.uporov.data.interactors.DebouncedQueryDirectionsInteractor
import d.uporov.data.interactors.QueryDirectionsInteractor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private const val AVIASALES_API_URL = "https://yasen.hotellook.com/"

val dataKoinModule = module {
    single {
        Retrofit.Builder()
            .baseUrl(AVIASALES_API_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(AviasalesApi::class.java)
    }
    single { QueryDirectionsInteractor(get()) }
    factory { DebouncedQueryDirectionsInteractor(get()) }
}