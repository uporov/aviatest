package d.uporov.domain

import androidx.lifecycle.MutableLiveData
import d.uporov.data.dataKoinModule
import d.uporov.domain.directions.DirectionsViewModel
import d.uporov.domain.search.SearchDirectionViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val ERROR_LIVE_DATA_QUALIFIER = "ERROR_LIVE_DATA_QUALIFIER"
const val NAVIGATION_LIVE_DATA_QUALIFIER = "NAVIGATION_LIVE_DATA_QUALIFIER"

private val domainKoinModule = module {
    plus(dataKoinModule)
    viewModel {
        MainViewModel(
            get(named(NAVIGATION_LIVE_DATA_QUALIFIER)),
            get(named(ERROR_LIVE_DATA_QUALIFIER))
        )
    }
    viewModel {
        DirectionsViewModel(
            get(named(NAVIGATION_LIVE_DATA_QUALIFIER)),
            get(named(ERROR_LIVE_DATA_QUALIFIER))
        )
    }
    viewModel {
        SearchDirectionViewModel(
            get(),
            get(named(NAVIGATION_LIVE_DATA_QUALIFIER)),
            get(named(ERROR_LIVE_DATA_QUALIFIER))
        )
    }
    single(named(NAVIGATION_LIVE_DATA_QUALIFIER)) {
        MutableLiveData<NavigationTarget?>().apply {
            value = Directions
        }
    }
    single(named(ERROR_LIVE_DATA_QUALIFIER)) { MutableLiveData<Throwable?>() }
}

val domainKoinModules = arrayOf(dataKoinModule, domainKoinModule)
