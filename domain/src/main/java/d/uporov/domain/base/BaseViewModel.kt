package d.uporov.domain.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

abstract class BaseViewModel(
    val errorLiveData: MutableLiveData<Throwable?>
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    final override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    protected fun Observable<*>.addSubscription() {
        subscribeOn(Schedulers.io())
            .subscribe({}, ::renderError)
            .run(compositeDisposable::add)
    }

    protected fun renderError(error: Throwable) {
        // common point to render chain errors
        errorLiveData.postValue(error)
    }

    fun errorWasRendered() {
        errorLiveData.value = null
    }
}