package d.uporov.domain.directions

enum class DirectionType {
    FROM, TO
}