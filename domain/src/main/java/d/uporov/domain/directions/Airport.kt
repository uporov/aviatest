package d.uporov.domain.directions

import d.uporov.data.model.directions.CitiesItem
import java.io.Serializable

data class Airport(
    val city: CitiesItem,
    val code: String
) : Serializable