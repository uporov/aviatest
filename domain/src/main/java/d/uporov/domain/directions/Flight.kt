package d.uporov.domain.directions

import java.io.Serializable

data class Flight(
    val from: Airport,
    val to: Airport
) : Serializable