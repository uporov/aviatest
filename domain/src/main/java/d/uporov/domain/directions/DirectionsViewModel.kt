package d.uporov.domain.directions

import androidx.lifecycle.MutableLiveData
import d.uporov.data.model.directions.CitiesItem
import d.uporov.domain.NavigationTarget
import d.uporov.domain.SearchDirection
import d.uporov.domain.WaitingMap
import d.uporov.domain.base.BaseViewModel

class DirectionsViewModel(
    private val navigationLiveData: MutableLiveData<NavigationTarget?>,
    errorLiveData: MutableLiveData<Throwable?>
) : BaseViewModel(errorLiveData) {

    val selectedFromDirectionLiveData = MutableLiveData<Airport>()
    val selectedToDirectionLiveData = MutableLiveData<Airport>()

    fun onDirectionChosen(type: DirectionType, item: CitiesItem, airportCode: String) {
        when (type) {
            DirectionType.FROM -> selectedFromDirectionLiveData.postValue(Airport(item, airportCode))
            DirectionType.TO -> selectedToDirectionLiveData.postValue(Airport(item, airportCode))
        }
    }

    fun searchDirection(type: DirectionType, requestCode: Int) {
        navigationLiveData.postValue(SearchDirection(type, requestCode))
    }

    fun findFlights() {
        checkDataAndNavigate(selectedFromDirectionLiveData.value, selectedToDirectionLiveData.value)
    }

    private fun checkDataAndNavigate(from: Airport?, to: Airport?) {
        when {
            from == null -> renderError(IncorrectFromDirectionException())
            to == null -> renderError(IncorrectToDirectionException())
            from.city == to.city -> renderError(FromAndToAreEqualException())
            else -> navigationLiveData.postValue(WaitingMap(Flight(from, to)))
        }
    }
}