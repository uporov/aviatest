package d.uporov.domain

import androidx.lifecycle.MutableLiveData
import d.uporov.domain.base.BaseViewModel

class MainViewModel(
    val navigationLiveData: MutableLiveData<NavigationTarget?>,
    errorLiveData: MutableLiveData<Throwable?>
): BaseViewModel(errorLiveData) {

    fun initTarget() {
        navigationLiveData.postValue(Directions)
    }

    fun onNavigationProcessed() {
        navigationLiveData.postValue(null)
    }
}