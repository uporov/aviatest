package d.uporov.domain.search

import d.uporov.data.model.directions.CitiesItem

sealed class SearchState

object Loading : SearchState()
data class SearchResult(val list: List<CitiesItem>) : SearchState()