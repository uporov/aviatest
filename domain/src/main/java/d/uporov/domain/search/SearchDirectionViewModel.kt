package d.uporov.domain.search

import androidx.lifecycle.MutableLiveData
import d.uporov.data.interactors.DebouncedQueryDirectionsInteractor
import d.uporov.data.model.directions.CitiesItem
import d.uporov.domain.ChooseAirport
import d.uporov.domain.NavigationTarget
import d.uporov.domain.base.BaseViewModel
import io.reactivex.Observable

class SearchDirectionViewModel(
    private val queryInteractor: DebouncedQueryDirectionsInteractor,
    private val navigationLiveData: MutableLiveData<NavigationTarget?>,
    errorLiveData: MutableLiveData<Throwable?>
) : BaseViewModel(errorLiveData) {

    val searchStateLiveData = MutableLiveData<SearchState>()
    private var lastQuery: String? = null

    init {
        searchStateLiveData.postValue(SearchResult(emptyList()))
        queryInteractor.resultObservable().subscribeOnResult()
    }

    private fun Observable<List<CitiesItem>>.subscribeOnResult() {
        map(::SearchResult)
            .doOnNext(searchStateLiveData::postValue)
            .doOnError {
                lastQuery = null
                queryInteractor.recreate().subscribeOnResult()
            }
            .addSubscription()
    }

    fun findDirection(query: String) {
        if (query != lastQuery) {
            searchStateLiveData.value = Loading
            queryInteractor(query)
        }
    }

    fun chooseAirport(citiesItem: CitiesItem, requestCode: Int) {
        navigationLiveData.postValue(ChooseAirport(citiesItem, requestCode))
    }
}