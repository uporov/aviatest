package d.uporov.domain

import d.uporov.data.model.directions.CitiesItem
import d.uporov.domain.directions.DirectionType
import d.uporov.domain.directions.Flight

sealed class NavigationTarget

object Directions : NavigationTarget()
data class SearchDirection(val directionType: DirectionType, val requestCode: Int) : NavigationTarget()
data class ChooseAirport(val citiesItem: CitiesItem, val requestCode: Int) : NavigationTarget()
data class WaitingMap(val flight: Flight): NavigationTarget()
